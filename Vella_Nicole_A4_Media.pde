/*
 Project:      A4: Media
 Student:      Nicole Vella
 Professor:    Geoffrey Shea
 Course:       GART 1036 - Art & Code - OCAD University
 Created on:   February 1, 2020
 Based on:     (nothing, this code is all my own)
 
 Title:        The Evolution of Truth
 */

// var for image
PImage img;

// let's start the drawing using 1x1 squares
int gridSize = 1;

void setup() {
  // set size of canvas
  size (560, 800);
  
  // load image into variable and resize it to the size of the canvas
  img = loadImage("trump.jpg");
  img.resize(width, height);
  
  // we don't want strokes on any of our shapes
  noStroke();
}

void draw() {

  // make a grid of pixels based on the grid size
  for (int y=0; y < width; y++) {
    for (int x=0; x < height; x++) {
      
      // get the colour of the pixel in the image at the location of each grid point
      fill(img.get(y*gridSize, x*gridSize));
      
      // draw a rect at each grid point at the current gridSize
      rect(y*gridSize, x*gridSize, gridSize, gridSize);
    }
  }
  
  // increade the grid size by 1 each loop, this will in effect pixelate
  // the image more and more, distorting it (reality/the truth) further and further. 
  gridSize+=1;
  
  // add a bit of a delay to slow down the process
  delay(1);
}
